<?php
include 'includes/data.php';

$menu_active = 'reservations';
include 'includes/head.php';
include 'includes/header.php';
?>
<main class="reservations">
	<div class="title">
		<div class="container">
			<ol class="breadcrumb hidden-xs hidde-sm">
				<li><a href="/" class="glyphicon glyphicon-home"><span class="sr-only">Home</span></a></li>
				<li class="active">Mis reservas</li>
			</ol>
			<h1>Mis reservas</h1>
		</div>
		<img src="assets/images/nurse-services.jpg">
	</div>
	<div class="container">
		<section class="reservations active">
			<h2 class="panel-title">Reservas actuales</h2>
			<article>
				<h3><i class="icon-calendar"></i>13 junio - 22 junio</h3>
				<p><i class="icon-here"></i>Hotel Plaza, Barcelona</p>
				<a href="" class="all-link"></a>
			</article>
			<article>
				<h3><i class="icon-calendar"></i>13 junio - 22 junio</h3>
				<p><i class="icon-here"></i>Hotel Plaza, Barcelona</p>
				<a href="" class="all-link"></a>
			</article>
			<article>
				<h3><i class="icon-calendar"></i>13 junio - 22 junio</h3>
				<p><i class="icon-here"></i>Hotel Plaza, Barcelona</p>
				<a href="" class="all-link"></a>
			</article>
		</section>
		<section class="reservations past">
			<h2>Reservas pasadas</h2>
			<article class="panel">
				<div class="panel-heading" role="tab" id="pastReservations">
					<a class="collapsed clearfix" data-toggle="collapse" data-parent="#pastReservations" href="#past-1" aria-expanded="false" aria-controls="collapseOne">
						<h3><i class="icon-calendar"></i>13 junio - 22 junio</h3>
						<p><i class="icon-here"></i>Hotel Plaza, Barcelona</p>
					</a>
				</div>
				<div id="past-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPersonalData">
					<h4>Datos de facturación</h4>
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-2 control-label">Nombre</label>
							<div class="col-md-10">
								<p class="form-control-static">Julio Marco Cuadrado</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Fecha de nacimiento</label>
							<div class="col-md-10">
								<p class="form-control-static">20/12/2015</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Idioma</label>
							<div class="col-md-10">
								<p class="form-control-static">Español</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Email</label>
							<div class="col-md-10">
								<p class="form-control-static">jmartidominguez@gmail.com</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Teléfono</label>
							<div class="col-md-10">
								<p class="form-control-static">(+34) 644 664 216</p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-6">
								<button class="btn btn-default btn-block">Editar datos de facturación</button>
							</div>
							<div class="col-xs-6">
								<button class="btn btn-default btn-block">Tramitar factura</button>
							</div>
						</div>
					</form>
				</div>
			</article>
			<article>
				<h3><i class="icon-calendar"></i>13 junio - 22 junio</h3>
				<p><i class="icon-here"></i>Hotel Plaza, Barcelona</p>
				<a href="" class="all-link"></a>
			</article>
		</section>
	</div>
</main>
<?php
include 'includes/footer.php';
?>