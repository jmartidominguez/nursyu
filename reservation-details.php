<?php
include 'includes/data.php';

$menu_active = 'reservations';
include 'includes/head.php';
include 'includes/header.php';
?>
<main class="details">
	<div class="title">
		<div class="container">
			<ol class="breadcrumb hidden-xs hidde-sm">
				<li><a href="/" class="glyphicon glyphicon-home"><span class="sr-only">Home</span></a></li>
				<li><a href="my-reservations.php">Mis reservas</a></li>
				<li class="active">Gestiona mi reserva</li>
			</ol>
			<h1>Gestiona tu reserva</h1>
		</div>
		<img src="assets/images/nurse-services.jpg">
	</div>
	<div class="container">
		<div class="row">
			<div id="details-accordion" role="tablist" aria-multiselectable="true" class="masonry-container clearfix js-masonry" data-masonry-options='{"itemsSelector": ".item"}'>
				<div class="col-xs-12 col-sm-6 item">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingPersonalData">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#details-accordion" href="#personal-data" aria-expanded="false" aria-controls="collapseOne">
									<span class="section-name">Datos del paciente</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="personal-data" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPersonalData">
							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-md-2 control-label">Nombre</label>
									<div class="col-md-10">
										<p class="form-control-static">Julio Marco Cuadrado</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Fecha de nacimiento</label>
									<div class="col-md-10">
										<p class="form-control-static">20/12/2015</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Idioma</label>
									<div class="col-md-10">
										<p class="form-control-static">Español</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Email</label>
									<div class="col-md-10">
										<p class="form-control-static">jmartidominguez@gmail.com</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Teléfono</label>
									<div class="col-md-10">
										<p class="form-control-static">(+34) 644 664 216</p>
									</div>
								</div>
								<button class="btn btn-primary btn-block">Cambiar lugar y servicios</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 item">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headerDates">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#details-accordion" href="#dates" aria-expanded="false" aria-controls="collapseOne">
									<span class="section-name">Lugar y fechas</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="dates" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerDates">
							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-md-2 control-label">Hotel</label>
									<div class="col-md-10">
										<p class="form-control-static">Hotel Costabella, Girona</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Inyectables o vacunas</label>
									<div class="col-md-10">
										<p class="form-control-static">20/12/2015, 21/12/2015, 29/07/15</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Control y seguimiento del paciente con O<sub>2</sub></label>
									<div class="col-md-10">
										<p class="form-control-static">20/12/2015, 21/12/2015, 29/07/15</p>
									</div>
								</div>
								<button class="btn btn-primary btn-block">Cambiar datos</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 item">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headerExtras">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#details-accordion" href="#extras" aria-expanded="false" aria-controls="collapseOne">
									<span class="section-name">Extras</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="extras" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerExtras">
							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-md-6 control-label">Servicio extra (30€)</label>
									<div class="col-md-6">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-6 control-label">Servicio extra (30€)</label>
									<div class="col-md-10">
										
									</div>
								</div>
								<button class="btn btn-primary btn-block">Cambiar servicios extras</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 item">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headerCancel">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#details-accordion" href="#cancel" aria-expanded="false" aria-controls="collapseOne">
									<span class="section-name">Cancelar reserva</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="cancel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerCancel">
							<form class="form-horizontal">
								<button class="btn btn-default btn-block">Cancelar reserva</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php
include 'includes/footer.php';
?>