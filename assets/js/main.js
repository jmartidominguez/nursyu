WebFontConfig = {
  google: { families: [ 'Source+Sans+Pro::latin' ] }
};
(function() {
  var wf = document.createElement('script');
  wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
  wf.type = 'text/javascript';
  wf.async = 'true';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(wf, s);
})();


// MOBILE MENÚ
if( $('#navbar-collapse').length ) {
  if( $( window ).width() < 768 ) {
    var navbar_position = $( '#navbar-collapse' ).position();
    $( '#navbar-collapse ').height( $( window ).height() - navbar_position.top );
  }

  $( window ).resize( function(){

    if( $( window ).width() < 768 ) {
      var navbar_position = $( '#navbar-collapse' ).position();
      $( '#navbar-collapse ').height( $( window ).height() - navbar_position.top );
    } else {
      $( '#navbar-collapse ').css( 'height' , 'auto' );  
    }
  });

  $('.menu-trigger').click(function(e){
    if( $( window ).width() < 768 ) {
      e.preventDefault();
    	if (!$('body').hasClass('showing-navbar') ) {
        $('body').addClass('showing-navbar').append('<div class="opacity-layer"></div>');
        $( '.opacity-layer' ).delay( 10 ).queue(function(next){
          $( this ).css({
            'background-color': 'rgba(0,0,0,0.4)',
            'transition': 'background-color 0.4s',
            '-webkit-transition': 'background-color 0.4s',
            'position': 'fixed',
            'z-index': 9,
            'top': 0,
            'right': 0,
            'left': 0,
            'bottom': 0
          });
          next();
        });

      } else {
        $( 'body' ).removeClass( 'showing-navbar' );
        $( '.opacity-layer' ).css('background-color', 'rgba(0,0,0,0)').delay( 400 ).queue(function(next){
          $( this ).remove();
          next();
        });
      }
    }
  });

  $( document ).click( function(e) {
      if( $( '.opacity-layer' ).is( e.target ) || $( '#navbar-collapse-trigger' ).is( e.target ) ) {
        $( 'body' ).removeClass( 'showing-navbar' );
        $( '.opacity-layer' ).css('background-color', 'rgba(0,0,0,0)').delay( 400 ).queue(function(next){
          $( this ).remove();
          next();
        });
      }
  });
}

////END NAVBAR MENU


//// SERVICES.PHP
  $( '.panel-item a' ).mouseenter( function(){
    $( this ).parent().addClass( 'over' );
  }).mouseleave( function(){
    $( this ).parent().removeClass( 'over' );
  })

  var btn_width = $( '.panel button.add' ).outerWidth();
  $( '.panel button.add' ).css({
    'width': btn_width,
    'padding-left': 0,
    'padding-right': 0,
  });
  $( '.product-list' ).on( 'click', 'button.add', function(){
    $btn = $(this);
    
    var product_name = $btn.siblings( 'a' ).children( '.product' ).text();
    var product_id = $btn.siblings( 'a' ).data( 'product-id' );
    var $li = $btn.closest( 'li.panel-item' );
    if( !$btn.hasClass( 'added' ) ) {
      var added_txt = $btn.data( 'added-text' );
      var default_txt = $btn.text();
      $btn.data( 'default-text', default_txt );
      $btn.addClass( 'added' );
      $btn.text( added_txt );
      var $cart_list = $( '.cart ul' );
      if( $cart_list.children( 'lh' ).hasClass( 'clear' ) ) {
        var services_added_text = $cart_list.data( 'services-added-text' );
        var $lh = $cart_list.children( 'lh' );
        $cart_list.data( 'no-services-text', $lh.text() );
        $cart_list.children( 'lh' ).text( services_added_text ).removeClass( 'clear' );
      }
      $( '.cart ul' ).append( '<li data-product-id="' + product_id + '">' + product_name + '</li>' );
      $li.addClass( 'added' );
      $li.find('.product').append( '<span class="glyphicon glyphicon-ok" aria-hidden="added"></span>' );
    } else {
      $btn.removeClass( 'added over' );
      var default_txt = $btn.data( 'default-text' );
      $btn.text( default_txt );
      $( '.cart ul' ).children( 'li[data-product-id="' + product_id + '"]' ).remove();
      $li.removeClass( 'added' );
      $li.find('.product').children('.glyphicon').remove();
      var $cart_list = $( '.cart ul' );
      if( !$cart_list.children( 'li' ).length ) {
        var no_services = $cart_list.data('no-services-text');
        $cart_list.children( 'lh' ).text( no_services ).addClass( 'clear' );
        
      }
    }
  })

  // Default added elements
  $( '.product-list li.added' ).each( function() {
    $( this ).find( '.product' ).append( '<span class="glyphicon glyphicon-ok" aria-hidden="added"></span>' );
    $btn = $( this ).children( 'button' );
    var added_txt = $btn.data( 'added-text' );
    var default_txt = $btn.text();
    $btn.data( 'default-text', default_txt );
    $btn.addClass( 'added' );
    $btn.text( added_txt );
    var product_name = $btn.siblings( 'a' ).children( '.product' ).text();
    var product_id = $btn.siblings( 'a' ).data( 'product-id' );
    var $cart_list = $( '.cart ul' );
    if( $cart_list.children( 'lh' ).hasClass( 'clear' ) ) {
      var services_added_text = $cart_list.data( 'services-added-text' );
      var $lh = $cart_list.children( 'lh' );
      $cart_list.data( 'no-services-text', $lh.text() );
      $cart_list.children( 'lh' ).remove();
      $cart_list.append( '<lh>' + services_added_text +'</lh>' );
    }
    $( '.cart ul' ).append( '<li data-product-id="' + product_id + '">' + product_name + '</li>' );
  });



  // Button over text
  $( 'button.add' )
    .mouseenter( function(){
      if( $( this ).hasClass( 'added' ) ) {
        var over_txt = $( this ).data( 'hover-text' );
        $( this ).text( over_txt );
        $( this ).addClass( 'over' );
      }
    })
    .mouseleave( function(){
      if( $( this ).hasClass( 'added' ) ) {
        var added_txt = $( this ).data( 'added-text' );
        $( this ).text( added_txt );
        $( this ).removeClass( 'over' );
      }
    })

  // Add number of items added in each section
  $( '.panel' ).each( function() {
    var items = $( this ).find( '.panel-item.added' ).length;
    if ( items > 0 ) {
      $( this ).find( '.section-name' )
        .append( '<span class="number">' + items + '</span>' );
    }
  })

  //Cart
  var products_added = $( '.panel-body li.added' ).size();



 //steps checkout
 if ( $( '.steps' ).hasClass( 'active-1' ) ) {
  var steps_border_w = $( '.step-1' ).outerWidth() + 'px';
 } else if ( $( '.steps' ).hasClass( 'active-2' ) ) {
  var steps_border_w = $( '.step-1' ).outerWidth() + $( '.step-2' ).outerWidth() + 'px' ;
 } else if ( $( '.steps' ).hasClass( 'active-3' ) ) {
  var steps_border_w = '100%';
 }
 $( 'body' ).append( '<style>.steps:after{width:' + steps_border_w + '}</style>' );


// calendar checkout
$('.has-calendar').datepicker({
  format: 'dd/mm/yy',
  startDate: '+1d',
  endDate: '+180d',
  multidate: true,
  multidateSeparator: ',',
  clearBtn: true,
  language: 'es',
  showOnFocus: false,
  disableTouchKeyboard: true,
})
  .on('show', function (e) {

    
  });

$('.icon-calendar').on('click', function () {
  calendarClicked = $(this);
  if($(this).hasClass('calendar-open')) {
    $(this).removeClass('calendar-open');
  } else {
    $(this).closest('article').find('.has-calendar').datepicker('show');
    $(this).addClass('calendar-open');
  }
});

$('.has-calendar').on('change', function () {
  var dates = $(this).val();
  dates = dates.split(',');
  var $target = $(this).siblings('.dates-selected');
  $target.empty();
  for(var i in dates) {
    $target.append('<span>' + dates[i] + '</span>');
  }
  if (dates.length == 0) {
    console.log($target);
    $target.text($target.data('text'));
  }
})
