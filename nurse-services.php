<?php
include 'includes/data.php';

$menu_active = 'services';
include 'includes/head.php';
include 'includes/header.php';
?>
<main class="services">
	<div class="title">
		<div class="container">
			<ol class="breadcrumb hidden-xs hidde-sm">
				<li><a href="/" class="glyphicon glyphicon-home"><span class="sr-only">Home</span></a></li>
				<li>Servicios</li>
				<li class="active">Servicios de enfermería</li>
			</ol>
			<h1>Servicio de enfermería</h1>
		</div>
		<img src="assets/images/nurse-services.jpg">
	</div>
	<div class="container">
		<div class="row">
			<section class="col-md-8 services-list">
				<div id="nurse-services-accordion" role="tablist" aria-multiselectable="true">
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingAdministration">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#nurse-services-accordion" href="#administration" aria-expanded="false" aria-controls="collapseOne">
									<span class="section-name">Administración de medicamentos</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="administration" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<ul class="panel-body list-unstyled product-list">
								<li class="panel-item added">
									<a href="product.php" data-product-id="01">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Inyectables o vacunas</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
								<li class="panel-item added">
									<a href="product.php" data-product-id="02">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Administración oral</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
								<li class="panel-item">
									<a href="product.php" data-product-id="03">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Nebulización</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingCures">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#nurse-services-accordion" href="#cures" aria-expanded="false" aria-controls="collapseTwo">
									<span class="section-name">Curas</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="cures" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<ul class="panel-body list-unstyled product-list">
								<li class="panel-item added">
									<a href="product.php" data-product-id="04">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Inyectables o vacunas</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
								<li class="panel-item">
									<a href="product.php" data-product-id="05">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Administración oral</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel">
						<div class="panel-heading" role="tab" id="headingBandage">
							<h4 class="panel-title">
								<a class="collapsed clearfix" data-toggle="collapse" data-parent="#nurse-services-accordion" href="#bandage" aria-expanded="false" aria-controls="collapseThree">
									<span class="section-name">Vendajes</span><span class="caret-green visible-xs"></span>
								</a>
							</h4>
						</div>
						<div id="bandage" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<ul class="panel-body list-unstyled product-list">
								<li class="panel-item">
									<a href="product.php" data-product-id="06">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Inyectables o vacunas</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
								<li class="panel-item">
									<a href="product.php" data-product-id="07">
										<span class="price hidden-xs">80€<span>por visita</span></span>
										<span class="product">Administración oral</span>
										
									</a>
									<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<aside class="col-md-4">
				<article class="cart with-services">
					<header>Tu reserva</header>
					<ul data-services-added-text="Servicios añadidos">
						<lh class="clear">No hay servicios añadidos</lh>
					</ul>
					<footer class="clearfix">
						<span class="hidden-md hidden-lg"><strong>1</strong><span>servicio(s)<br />añadidos</span></span>
						<a class="btn btn-primary icon-caret-right">Reservar</a>
					</footer>
				</article>
				<article class="hidden-xs hidden-sm">
					<header>Ventajas</header>
					<ul>
						<li>Te atenderá personal cualificado.</li>
						<li>Podrás elegir donde quieres que te atiendan.</li>
						<li>Paga una vez se te hayan realizado todos los tratamientos.</li>
					</ul>
				</article>
			</aside>
		</div>
	</div>
</main>
<?php
include 'includes/footer.php';
?>