<?php
include 'includes/data.php';

include 'includes/head.php';

$checkout = true;
$icon_back_link = 'nurse-services.php';
include 'includes/header.php';
?>
<div class="container">
	<ol class="steps clearfix active-1">
		<li class="step-1 active">Datos</li>
		<li class="step-2">Personalización</li>
		<li class="step-3">Pago</li>
	</ol>
	<div class="row">
		<main class="checkout col-md-8">
			<form>
				<section class="patient field">
					<h1>Datos del paciente</h1>
					<div class="form-group">
						<label for="name" class="control-label">Nombre</label>
						<input type="text" id="name" class="form-control" placeholder="Introduzca su nombre">
					</div>
					<div class="form-group">
						<label for="surname" class="control-label">Apellidos</label>
						<input type="text" id="surname" class="form-control" placeholder="Introduzca su apellido">
					</div>
					<fieldset class="birthday">
						<legend>Fecha de nacimiento</legend>
						<div class="form-group">
							<label for="birth-day" class="control-label sr-only">Día de nacimiento</label>
							<input type="text" id="birth-day" class="form-control" size="4" maxlength="2">
						</div>
						<div class="form-group">
							<label for="birth-month" class="control-label sr-only">Mes de nacimiento</label>
							<input type="text" id="birth-month" class="form-control" size="4" maxlength="2">
						</div>
						<div class="form-group">
							<label for="birth-year" class="control-label sr-only">Año de nacimiento</label>
							<input type="text" id="birth-year" class="form-control" size="4" maxlength="4">
						</div>
					</fieldset>
					<div class="form-group">
						<label for="surname" class="control-label">Nacionalidad</label>
						<input type="text" id="surname" class="form-control" placeholder="Introduzca su nacionalidad">
					</div>
					<fieldset>
						<legend>Idioma</legend>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-success active" for="spanish">
								<input type="radio" name="options" id="spanish" autocomplete="off" checked> ES
							</label>
							<label class="btn btn-success" for="catalan">
								<input type="radio" name="options" id="catalan" autocomplete="off"> CA
							</label>
							<label class="btn btn-success" for="english">
								<input type="radio" name="options" id="english" autocomplete="off"> EN
							</label>
							<label class="btn btn-success" for="french">
								<input type="radio" name="options" id="french" autocomplete="off"> FR
							</label>
							<label class="btn btn-success" for="german">
								<input type="radio" name="options" id="german" autocomplete="off"> DE
							</label>
						</div>
					</fieldset>
				</section>
				<section>
					<h1>Datos de contacto</h1>
					<div class="form-group has-error has-feedback">
						<label for="email" class="control-label">Email</label>
						<input type="email" id="email" class="form-control" placeholder="Introduzca su email" aria-describedby="email-error">
						<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
						<span id="email-error" class="form-control-error">Email no válido</span>
					</div>
					<fieldset class="phone-number">
						<legend>Teléfono</legend>
						<div class="form-group prefix">
							<label for="prefix" class="control-label"></label>
							<input type="text" id="prefix" class="form-control" maxlength="3" >
						</div>
						<div class="form-group">
							<label for="number" class="control-label"></label>
							<input type="text" id="number" class="form-control" aria-describedby="helpPhone" maxlength="12">
						</div>
						<span id="helpPhone" class="help-block">Lo necesitamos para contactar contigo</span>
					</fieldset>
				</section>
				<div class="button-container">
					<button type="submit" class="btn btn-primary btn-block">Ir a personalización</button>
				</div>
			</form>
		</main>
		<aside class="col-md-4">
				<article class="cart with-services">
					<header>Tu reserva</header>
					<ul data-services-added-text="Servicios añadidos">
						<lh class="clear">No hay servicios añadidos</lh>
					</ul>
				</article>
				<article class="hidden-xs hidden-sm">
					<header>Ventajas</header>
					<ul>
						<li>Te atenderá personal cualificado.</li>
						<li>Podrás elegir donde quieres que te atiendan.</li>
						<li>Paga una vez se te hayan realizado todos los tratamientos.</li>
					</ul>
				</article>
			</aside>
	</div>
</div>

<?php
include 'includes/footer.php';
?>