<?php
include 'includes/data.php';

$menu_active = 'services';
include 'includes/head.php';

$checkout = true;
$icon_back_link = 'nurse-services.php';
include 'includes/header.php';
?>
<div class="container">
	<ol class="steps clearfix active-3">
		<li class="step-1">Datos</li>
		<li class="step-2">Personalización</li>
		<li class="step-3 active">Pago</li>
	</ol>
	<div class="row">
		<main class="checkout col-md-8">
			<form>
				<section class="reservation">
					<h1>Reserva</h1>
					<p>La reserva se te descontará del precio del servicio</p>
					<h2>Reserva del servicio: 39€</h2>
				</section>
				<section class="card clearfix">
					<h1>Datos de la tarjeta</h1>
					<div class="form-group">
						<label for="card-type" class="control-label">Titular de la tarjeta</label>
						<select id="card-type" class="form-control input-lg">
							<option selected disabled>Seleccione tipo de tarjeta</option>
							<option value="visa">Visa</option>
							<option value="mastercard">Mastercard</option>
							<option value="aexpress">American Express</option>
						</select>
					</div>
					<div class="form-group">
						<label for="card-owner" class="control-label">Titular de la tarjeta</label>
						<input type="text" id="card-owner" class="form-control input-lg">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="card-number" class="control-label">Número de la tarjeta</label>
								<input type="text" id="card-owner" class="form-control input-lg" size="30">
							</div>
							<div class="form-group clearfix">
								<fieldset class="card-before lfloat">
									<legend class="pad0">Fecha de caducidad</legend>
									<label for="month" class="control-label sr-only">Mes de caducidad</label>
									<select id="month" class="form-control input-lg">
										<option>01</option>
										<option>02</option>
										<option>03</option>
										<option>04</option>
										<option>05</option>
										<option>06</option>
										<option>07</option>
										<option>08</option>
										<option>09</option>
										<option>10</option>
										<option>11</option>
										<option>12</option>	
									</select>
									<label for="year" class="control-label sr-only">Año de caducidad</label>
									<select id="month" class="form-control input-lg">
										<option>15</option>
										<option>16</option>
										<option>17</option>
										<option>18</option>
										<option>19</option>
										<option>20</option>
										<option>21</option>
										<option>22</option>
									</select>
								</fieldset>
								<div class="ccv">
									<label for="ccv" class="control-label">CCV</label>
									<input type="text" class="form-control auto input-lg" size="3">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<img class="secure-payment" src="assets/images/pago_seguro.png">
						</div>
					</div>
					<div class="button-container col-md-12">
						<div class="checkbox">
							<label>
								<input type="checkbox">Acepto las <a href="javascript:void(0)">Condiciones de uso</a> y la <a href="javascript:void(0)">Política de privacidad</a>
							</label>
						<button type="submit" class="btn btn-primary btn-block">Pagar ahora: 39€</button>
						<p class="legal">Al hacer click en el botón <em>pagar ahora</em>, aceptas las <a href="javascript:void(0)">Condiciones de uso</a> y la <a href="javascript:void(0)">Política de privacidad</a>.</p>
					</div>
				</section>
			</form>
		</main>
		<aside class="checkout col-md-4 hidden-xs hidden-sm">
			<article class="cart with-services clearfix">
				<header>Resumen</header>
				<ul data-services-added-text="Servicios añadidos">
					<li class="row">
						<span class="col-md-8 col-lg-9">
							<span>Control y seguimiento del paciente con O2</span>
							<span class="gray-text">22/03/14 - 24/03/14</span>
						</span>
						<span class="price col-md-4 col-lg-3">
							<span class="rfloat"><span class="quantity">2 x</span> 80<span class="currency">€</span></span>
						</span>
					</li>
					<li class="row">
						<span class="col-md-9">
							Servicio extra
						</span>
						<span class="col-md-3">
							<span class="rfloat">30€</span>
						</span>
					</li>
				</ul>
				<div class="col-xs-12 total">
					<span class="lfloat">Total</span>
					<span class="rfloat">190€</span>
				</div>
			</article>
			<article class="hidden-xs hidden-sm">
				<header>Ventajas</header>
				<ul>
					<li>Te atenderá personal cualificado.</li>
					<li>Podrás elegir donde quieres que te atiendan.</li>
					<li>Paga una vez se te hayan realizado todos los tratamientos.</li>
				</ul>
			</article>
		</aside>
	</div>
</div>

<?php
include 'includes/footer.php';
?>