<header>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <?php
        if ( $homepage == true ) : ?>
          <h1><span class="navbar-brand-container"><a class="navbar-brand menu-trigger" href="index.php">Nursyu</a></span></h1>
        <?php elseif ( $icon_back == true ): ?>
          <span class="navbar-brand-container back"><a class="back" href="<?php echo $icon_back_link?>"><span></span></a><span class="navbar-brand" href="/">
            Nursyu</span></span>
        <?php elseif ( $checkout == true ) : ?>
          <span class="navbar-brand-container checkout"><span class="navbar-brand" href="/">
            Nursyu</span></span>
        <?php else : ?>
          <span class="navbar-brand-container"><a class="navbar-brand menu-trigger" href="index.php">
            Nursyu</a></span>
        
        <?php endif; ?>
      </div>
      <?php
      if ( $checkout != true ) : ?>
        <div id="navbar-collapse">
          <ul class="nav navbar-nav">
            <?php if ($menu_active == 'home') :
              echo '<li class="active visible-xs"><a href="index.php">Inicio<span class="sr-only">(current)</span></a></li>';
            else :
              echo '<li class="visible-xs"><a href="index.php">Inicio</a></li>';
            endif;
            if ($menu_active == 'services') :
              echo '<li class="active"><a href="nurse-services.php">Servicios<span class="sr-only">(current)</span></a></li>';
            else :
              echo '<li><a href="nurse-services.php">Servicios</a></li>';
            endif;
            if ($menu_active == 'reservations') :
              echo '<li class="active"><a href="my-reservations.php">Mis Reservas<span class="sr-only">(current)</span></a></li>';
            else :
              echo '<li><a href="my-reservations.php">Mis Reservas</a></li>';
            endif;
            if ($menu_active == 'about') :
              echo '<li class="active"><a href="about-us.php">Quiénes somos<span class="sr-only">(current)</span></a></li>';
            else :
              echo '<li><a href="reservations.php">Quiénes somos</a></li>';
            endif; ?>
          </ul>
          <div class="navbar-right">
            <form class="navbar-form clearfix" role="form">
              <div class="form-group">
                <input type="text" placeholder="Buscar" class="form-control">
                <button type="submit" class="btn btn-default"><span class="sr-only">Buscar</span></button>
              </div>
            </form>
            <ul class="nav navbar-nav language">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">ES <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">EN (English)</a></li>
                </ul>
              </li>
              <li class="visible-xs"><a href="#">Aviso legal</a></li>
            </ul>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </nav>
</header>