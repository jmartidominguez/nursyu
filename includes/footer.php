<footer class="hidden-xs footer-page">
	<div class="container">
		<img src="assets/images/logo.png" width="100">
  		<p class="text-center">&copy; Nursyu, 2015<?php echo ($checkout != true) ? '- <a href="#">Aviso legal</a>' : ''?></p>
  	</div>
</footer>
<script src="assets/js/vendor/jquery-1.11.1.min.js"></script>
<script src="assets/js/jquery-ui-1.11.1.js"></script>
<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/locales/bootstrap-datepicker.es.min.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>


<script src="assets/js/plugins.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
if ( $( window ).width() < 768 ) {
  $( '#navbar' ).css( 'max-height', $( window ).height() - $( '.navbar-header' ).height() ) ;  
}
$( window ).resize(function(){
  if ( $( window ).width() < 768 ) {
    $( '#navbar' ).css( 'max-height', $( window ).height() - $( '.navbar-header' ).height() ) ;  
  }  
})

</script>
</body>
</html>