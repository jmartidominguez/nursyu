
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="assets/css/jquery-ui.css">
  <link rel="stylesheet" href="assets/css/jquery-ui.structure.css">
  <link rel="stylesheet" href="assets/css/jquery-ui.theme.css">
  <link rel="stylesheet" href="assets/css/bootstrap-datepicker.css">
  
  <link rel="stylesheet" href="assets/css/main.css">



  <script src="assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body<?php echo ($homepage == true) ? ' class="homepage"' : ''?>>
<!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->