<?php
include 'includes/data.php';

$homepage = true;
include 'includes/head.php';
include 'includes/header.php';
?>
<main>
  <section class="above-fold">
    <div class="container">
      <video autoplay loop poster="" id="bgvid" class="hidden-xs">
        <source src="assets/video/home-video.webm" type="video/webm">
        <source src="assets/video/home-video.mp4" type="video/mp4">
      </video>
      <h2 class="lead">Viaja tranquilo con un equipo sanitario a tu servicio</h2>
    </div>
    <div class="finder">
      <div class="container">
        <div class="row">
          <form>
            <div class="form-group col-sm-4">
              <label for="professional">Profesional</label>
              <div class="select-style">
                <select class="form-control input-lg" id="professional">
                  <option>Enfermería</option>
                  <option>Fisioterapia</option>
                </select>
              </div>
            </div>
            <div class="form-group col-sm-4">
              <label for="where">Dónde</label>
              <div class="select-style">
                <select class="form-control input-lg" id="where">
                  <option>Girona (Spain)</option>
                </select>
              </div>
            </div>
            <div class="col-sm-4 col-md-4">
              <button type="submit" class="btn btn-lg btn-primary btn-block">Reserva ahora</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="advantages">
    <div class="container">
      <hgroup class="col-xs-12">
        <h2>¿Cómo funciona?</h2>
      </hgroup>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <i class="icon-reservation"></i>
          <h3>Reserva un servicio</h3>
          <p>De un modo fácil y rápido, puede reservar las fechas y el tipo de servicio con el profesional que necesite.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <i class="icon-letter"></i>
          <h3>Un profesional le contactará</h3>
          <p>Recibirá la aceptación del servicio a través de email.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <i class="icon-plane"></i>
          <h3>Viaje tranquilo</h3>
          <p>Ya puede disfrutar de su tiempo sabiendo que usted y las personas que le importan estarán bien atendidas.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="reasons">
    <div class="container">
      <hgroup class="col-xs-12">
        <h2>¿Por qué elegirnos?</h2>
      </hgroup>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <i class="icon-badge"></i>
          <h3>Personal cualificado</h3>
          <p>Apostamos por profesionales altamente cualificados y con formación continua. Todos ellos cuentan con colegiación obligatoria en aquellas profesiones que lo requieras.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <i class="icon-dialog"></i>
          <h3>Atención personalizada</h3>
          <p>Serán atendido por el mismo profesional a la hora y lugar que usted desee, lo que facilita el proceso de cuidados individualizados.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <i class="icon-clock"></i>
          <h3>Ahorre tiempo</h3>
          <p>No se tendrá que desplazar ni esperar, el profesional vendrá hasta donde usted precise sin horarios ni restricciones.</p>
        </div>
      </div>
    </div>
  </section>
  <section class="partners">
    <div class="container">
      <hgroup class="col-xs-12">
        <h2>¿Con quién trabajamos?</h2>
      </hgroup>
      <div class="row text-center">
        <img src="assets/images/partners/nh-logo.svg">
        <img src="assets/images/partners/melia-logo.png">
        <img src="assets/images/partners/carlemany-logo.jpg">
      </div>
    </div>
  </section>
  <section class="testionials">
    <div class="container">
      <hgroup class="col-xs-12">
        <h2>Opiniones</h2>
      </hgroup>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <span class="satisfaction">95%</span>
          <span>de satisfacción entre nuestros clientes</span>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <div class="opinion">
            <p>“Gracias a Nursyu pude disfrutar de mis vacaciones sin renunciar a unos cuidados excelentes”</p>
            <p class="author">Teresa Arjona</p>
          </div>
          <div class="opinion">
            <p>“Tenia que asistir a un evento de trabajo pero no podía renunciar a cuidar a mi padre"</p>
            <p class="author">Laura Mora</p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0">
          <div class="opinion">
            <p>“Realicé mis ejercicios de rehabilitación en mi habitación de hotel sin desplazamientos ni perder tiempo”</p>
            <p class="author">Carlos Herrera</p>
          </div>
          <div class="opinion">
            <p>“Los niños estuvieron atendidos en todo momento”</p>
            <p class="author">Helen Hightower</p>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<?php
include 'includes/footer.php';
?>