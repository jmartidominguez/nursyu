<?php
include 'includes/data.php';

include 'includes/head.php';

$checkout = true;
$icon_back_link = 'nurse-services.php';
include 'includes/header.php';
?>
<div class="container">
	<ol class="steps clearfix active-2">
		<li class="step-1">Datos</li>
		<li class="step-2 active">Personalización</li>
		<li class="step-3">Pago</li>
	</ol>
	<div class="row">
		<main class="checkout col-md-8">
			<form>
				<section class="place field tabpanel">
					<h1>Lugar</h1>
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#hotel-fields" aria-controls="home" role="tab" data-toggle="tab">Hotel</a></li>
						<li role="presentation"><a href="#apartment-fields" aria-controls="home" role="tab" data-toggle="tab">Apartamento</a></li>
					</ul>
					<div class="tab-content">
						<div id="hotel-fields" role="tabpanel" class="tab-pane active">
							<div class="form-group">
								<label for="hotel-name" class="control-label">Hotel</label>
								<input type="text" id="hotel-name" class="form-control" placeholder="Introduzca el nombre del hotel">
							</div>
						</div>
						<div id="apartment-fields" role="tabpanel" class="tab-pane">
							<div class="form-group">
								<label for="street" class="control-label">Dirección</label>
								<input type="text" id="street" class="form-control" placeholder="Introduzca la dirección">
							</div>
							<div class="form-group">
								<label for="street" class="control-label">Municipio</label>
								<input type="text" id="street" class="form-control" placeholder="Introduzca el municipio">
							</div>
						</div>
					</div>
				</section>
				<section class="dates field">
					<h1>Seleccionar fechas</h1>
					<article class="clearfix">
						<div class="service col-xs-9 col-sm-10">
							<p><strong>Inyectables o vacunas</strong></p>
							<span class="dates-selected" data-text="Sin días marcados">Sin días marcados</span>
							<label for="calendar1" class="sr-only">Selecciona fechas para inyectables o vacunas</label>
							<input type="text" id="calendar1" class="has-calendar sr-only">
						</div>
						<div class="col-xs-3 col-sm-2 text-right">
							<a href="#" class="icon-calendar"></a>
						</div>
					</article>
					<article class="clearfix">
						<div class="service col-xs-9 col-sm-10">
							<p><strong>Control y seguimiento del paciende con O2</strong></p>
							<span class="dates-selected" data-text="Sin días marcados">Sin días marcados</span>
							<label for="calendar1" class="sr-only">Selecciona fechas para inyectables o vacunas</label>
							<input type="text" id="calendar1" class="has-calendar sr-only">
						</div>
						<div class="col-xs-3 col-sm-2 text-right">
							<a href="#" class="icon-calendar"></a>
						</div>
					</article>
				</section>
				<div class="button-container">
					<button type="submit" class="btn btn-primary btn-block">Ir a pago</button>
				</div>
			</form>
		</main>
		<aside class="col-md-4">
				<article class="cart with-services">
					<header>Tu reserva</header>
					<ul data-services-added-text="Servicios añadidos">
						<lh class="clear">No hay servicios añadidos</lh>
					</ul>
				</article>
				<article class="hidden-xs hidden-sm">
					<header>Ventajas</header>
					<ul>
						<li>Te atenderá personal cualificado.</li>
						<li>Podrás elegir donde quieres que te atiendan.</li>
						<li>Paga una vez se te hayan realizado todos los tratamientos.</li>
					</ul>
				</article>
			</aside>
	</div>
</div>

<?php
include 'includes/footer.php';
?>