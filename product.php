<?php
include 'includes/data.php';

$menu_active = 'services';
include 'includes/head.php';

$icon_back = true;
$icon_back_link = 'nurse-services.php';
include 'includes/header.php';
?>

<main class="product-page">
	<div class="title">
		<div class="container">
			<ol class="breadcrumb hidden-xs hidde-sm">
				<li><a href="/" class="glyphicon glyphicon-home"><span class="sr-only">Home</span></a></li>
				<li>Servicios</li>
				<li><a href="nurse-services.php">Servicios de enfermería</a></li>
				<li>Administración de medicamentos</li>
				<li class="active">Inyecciones o vacunas</li>
			</ol>
			<h1>Inyecciones o vacunas</h1>
		</div>
	<img src="assets/images/nurse-services.jpg">
	</div>
	<div class="container">
		<div class="row">
			<section class="col-md-8 product">
				<div class="row">
					<article class="col-xs-12 product-card">
						<div class="clearfix">
							<div class="col-sm-6 description">
								<h2>Descripción</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat odio at augue efficitur porta. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent tincidunt interdum turpis, id sagittis sapien efficitur vel.</p>
								<ul class="list-unstyled included">
									<lh>Incluye</lh>
									<li>Material de inyección</li>
								</ul>
								<ul class="list-unstyled">
									<lh>No incluye</lh>
									<li>Medicamento o vacuna</li>
								</ul>
							</div>
							<div class="col-sm-6 footer-product">
								<img class="hidden-xs" src="assets/images/dummy/img-servei.jpg">
								<div class="price">
									<span>80€</span>
									<span>por visita</span>
								</div>
								<button class="btn btn-primary btn-block">Añadir servicio</button>
							</div>
						</div>
					</article>
					<article class="related-products col-xs-12 hidden-xs">
						<h4>Servicios relacionados</h4>
						<ul class="list-unstyled product-list">
							<li class="panel-item">
								<a href="product.php" data-product-id="01">
									<span class="price hidden-xs">80€<span>por visita</span></span>
									<span class="product">Inyectables o vacunas</span>
									
								</a>
								<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
							</li>
							<li class="panel-item">
								<a href="product.php" data-product-id="02">
									<span class="price hidden-xs">80€<span>por visita</span></span>
									<span class="product">Administración oral</span>
								</a>
								<button class="add btn btn-default hidden-xs hidden-sm" data-added-text="Añadido" data-hover-text="Eliminar">Añadir</button>
							</li>
						</ul>
					</article>
				</div>
			</section>
			<aside class="col-md-4">
				<article class="cart with-services">
					<header>Tu reserva</header>
					<ul data-services-added-text="Servicios añadidos">
						<lh class="clear">No hay servicios añadidos</lh>
					</ul>
				</article>
				<article class="hidden-xs hidden-sm">
					<header>Ventajas</header>
					<ul>
						<li>Te atenderá personal cualificado.</li>
						<li>Podrás elegir donde quieres que te atiendan.</li>
						<li>Paga una vez se te hayan realizado todos los tratamientos.</li>
					</ul>
				</article>
			</aside>
		</div>
	<div>
</main>



<?php
include 'includes/footer.php';
?>